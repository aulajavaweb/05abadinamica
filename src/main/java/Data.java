import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "data")
@ViewScoped
public class Data implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date hora;

	public Date getHora() {
		setHora();
		return hora;
	}

	public void setHora() {
		this.hora = new Date();
	}

}
